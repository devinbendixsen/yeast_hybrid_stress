# Yeast_Hybrid_Stress

Zhang Z, Bendixsen DP, Janzen T, Nolte AW, Greig D, Stelkens R (2019) Recombining your way out of trouble: The genetic architecture of hybrid fitness under environmental stress. Molecular Biology and Evolution mxz211. https://doi.org/10.1093/molbev/msz211 \
This directory is a library that contains data (.txt) files and Python scripts that were used in the data analysis of high-througput sequencing data of the genetic architecture of yeast hybrid genomes exposed to ten stressful environments. Raw sequencing data is deposited at the European Nucleotide Archive (ENA) Project No: **PRJEB33368**.


## Installation

The directory is formatted as spyder python project.


## Usage

Python scripts in this directory encompass four categories:\
**(1)** Scripts to re-create figures in Zhang, Bendixsen et al. (in review).\
**(2)** Scripts to analyze data of yeast hybrid genomes in different stressful environments.\
**(3)** Scripts to analyze the hybrid genomes grown without stress from published data (Kao et al. 2010).\
**(4)** Scripts to determine the expected range of measurements using simulated chromosomes assuming no selection and free segregation of chromosomes in hybrid genomes.
    
Data in this directory depict the analyzed data from high-throughput sequencing of hybrid genomes in 10 different environments. Raw sequencing data was analyzed by Zebin Zhang using custom R scripts.

Figures presented in Zhang, Bendixsen et al. (in review) are found in the Figures directory.

## Authors and acknowledgment

Python scripts were developed by **Devin Bendixsen, PhD** in collaboration with **Rike Stelkens, PhD** and **Zebin Zhang, PhD**.