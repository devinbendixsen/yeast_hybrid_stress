#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 11:02:35 2019

@author: devin
"""
# =============================================================================
# code to use data from Bernardes et al. 2016 JEB. The code re-plots the heteroisis
# plot and combines this data with the chromosome hybridity data.
# =============================================================================

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% Import 'fitness' data for hybrid vs parents in different environments (Bernardes et al. 2016 JEB)
dataset=pd.read_csv("../DATA/TableS2_JEB_2016.csv",delimiter=",")

#%% Plot the heterosis data from Bernandes et al. 2016 JEB
plt.figure(1,figsize=[12,4])
cmap = plt.cm.get_cmap('RdBu_r')
order=['Lith','EtOH','Nipagin','DMSO','Caffeine','CitricAcid','Peroxide','NaCl','Zinc','Aspirin']
sns.stripplot(x='environment',y='SC',color=cmap(0.9),data=dataset,linewidth=1,s=8,alpha=0.4,marker='d',dodge=True,order=order)
sns.pointplot(x='environment',y='SC',color=cmap(0.9),data=dataset,linewidth=1,join=False,markers='d',order=order,ci='sd')
sns.stripplot(x='environment',y='SP',color=cmap(0.1),data=dataset,linewidth=1,s=8,alpha=0.4,marker='d',dodge=True,order=order)
sns.pointplot(x='environment',y='SP',color=cmap(0.1),data=dataset,linewidth=1,join=False,markers='d',order=order,ci='sd')

plt.text(s='Hybrid vs. S. cerevesiae',color=cmap(0.9),x=4.5,y=0.55,horizontalalignment='center')
plt.text(s='Hybrid vs. S. paradoxus',color=cmap(0.1),x=4.5,y=0.5,horizontalalignment='center')

x=['Lithium\nAcetate','Ethanol','Nipagin','DMSO','Caffeine','Citric\nAcid','Hydrogen\nPeroxide','NaCl','Zinc\nSulfate','Salicylic\nAcid']
plt.xticks(range(0,10),x)
plt.xlabel('environment',fontsize=12,weight='bold')
plt.ylabel('heterosis',fontsize=12,weight='bold')
sns.despine(trim=True)
plt.tight_layout()
plt.savefig('../Figures/RADseq_environment_JEB_heterosis.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% Plot the relationsihp between parental phenotypic divergence and chromosome hybridity

dataset=pd.read_csv("../DATA/RADseq_heterosis-hybridity.csv",delimiter=",")

plt.figure(2,figsize=[12,12])
chr_num={1:dataset.chr1,2:dataset.chr2,3:dataset.chr3,4:dataset.chr4,5:dataset.chr5,6:dataset.chr6,7:dataset.chr7,8:dataset.chr8,9:dataset.chr9,10:dataset.chr10,11:dataset.chr11,12:dataset.chr12,13:dataset.chr13,14:dataset.chr14,15:dataset.chr15,16:dataset.chr16}
for i in range(1,17):
    plt.subplot(4,4,i)
    plt.scatter(dataset.heterosis,chr_num[i],c=chr_num[i],cmap='RdBu_r',linewidth=1,edgecolor='black',vmin=0,vmax=1)
    plt.vlines(0,ymin=0,ymax=1,linestyle='dashed')
    plt.ylim(0,1)
    plt.xlim(-0.42,0.42)
    plt.xticks([-.4,-.2,0,.2,.4])
    plt.title('chr {}'.format(i),weight='bold',fontsize=12)

plt.subplot(441)
plt.xlabel('parental phenotypic\ndivergence (Sc-Sp)',weight='bold')
plt.ylabel('chromosome hybridity',weight='bold')
plt.text(s='Sc',x=-0.25,y=0.1,horizontalalignment='center',color=cmap(0.9))
plt.text(s='Sp',x=0.25,y=0.1,horizontalalignment='center',color=cmap(0.1))
plt.tight_layout()
plt.savefig('../Figures/RADseq_heterosis_hybridity.png',bbox_inches='tight',dpi=1000,transparent=True)