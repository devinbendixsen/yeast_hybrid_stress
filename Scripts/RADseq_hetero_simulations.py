#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 19:50:15 2019

@author: devin
"""
# =============================================================================
# Calculate and plot the zygosity of simulated chromosomes based
# on no chromosomal interactions (no epistasis)
# =============================================================================
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import random
import pandas as pd
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% import and calculate the distribution of heterozygosity/ homozygosity
def enviroplot(Environment):
    dataset=pd.read_csv("../DATA/Diploidy_ChromosomeLevl.txt",delimiter="\t")
    enviroslice=dataset['Environment']==Environment
    dataset=dataset[enviroslice]
    x=dataset.Hetero/(dataset.N17+dataset.S288c+dataset.Hetero)
    y=dataset.S288c/(dataset.N17+dataset.S288c+dataset.Hetero)
    z=dataset.N17/(dataset.N17+dataset.S288c+dataset.Hetero)
    return x,y,z

envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
data_hetero={}
data_cer={}
data_par={}
for env in envs:
    data_hetero[env],data_cer[env],data_par[env]=enviroplot(env)

hetero_mean=[]
cer_mean=[]
par_mean=[]
for env in envs:
    hetero_mean.append(np.mean(data_hetero[env]))
    cer_mean.append(np.mean(data_cer[env]))
    par_mean.append(np.mean(data_par[env]))

#%% calculate and plot the distribution of zygosity of simualted chromosomes
#   compare and plot against the distribution of zygosity in our data
plt.figure(figsize=[12,9])
colors={}
gen_means=[]
par_means=[]
cer_means=[]
for i in range(0,10000):
    chromtotal=[]
    genome=[]
    par=[]
    cer=[]
    for i in range(0,24):
        x={}
        x['cer']=0
        x['par']=0
        x['hetero']=0
        chrom=[]
        for j in range(0,16):
            num=random.uniform(0,1)
            if num>=0.75:
                x['cer']+=1
            elif num<=0.25:
                x['par']+=1
            else:
                x['hetero']+=1
        par.append(x['cer']/16)
        cer.append(x['par']/16)
        genome.append(x['hetero']/16)
    plt.subplot(232)
    cm = plt.cm.get_cmap('Greys')
    sns.kdeplot(genome,color=cm(random.uniform(0.2,1)),bw=0.05)
    plt.ylim(0,6)
    plt.xlim(0,1)
    plt.title('heterozygous',weight='bold',fontsize=12)
    plt.subplot(231)
    sns.kdeplot(cer,color=cm(random.uniform(0.2,1)),bw=0.05)
    plt.ylim(0,6)
    plt.xlim(0,1)
    plt.ylabel('genomes',weight='bold',fontsize=12)
    plt.title('S. cerevisiae',weight='bold',fontsize=12)
    plt.subplot(233)
    sns.kdeplot(par,color=cm(random.uniform(0.2,1)),bw=0.05)
    plt.ylim(0,6)
    plt.xlim(0,1)
    plt.title('S. paradoxus',weight='bold',fontsize=12)
    gen_means.append(np.mean(genome))
    par_means.append(np.mean(par))
    cer_means.append(np.mean(cer))

for env in envs:
    plt.subplot(235)
    cm = plt.cm.get_cmap('Greys')
    sns.kdeplot(data_hetero[env],color=cm(random.uniform(0.2,1)),bw=0.05,shade=True)
    plt.xlim(0,1)
    plt.ylim(0,5)
    plt.xlabel('zygosity',weight='bold',fontsize=12)
    plt.subplot(234)
    cm = plt.cm.get_cmap('Reds')
    sns.kdeplot(data_cer[env],color=cm(random.uniform(0.2,1)),bw=0.05,shade=True)
    plt.xlim(0,1)
    plt.ylim(0,5)
    plt.ylabel('genomes',weight='bold',fontsize=12)
    plt.xlabel('zygosity',weight='bold',fontsize=12)
    plt.subplot(236)
    cm = plt.cm.get_cmap('Blues')
    sns.kdeplot(data_par[env],color=cm(random.uniform(0.2,1)),bw=0.05,shade=True)
    plt.xlim(0,1)
    plt.ylim(0,5)
    plt.xlabel('zygosity',weight='bold',fontsize=12)
sns.despine(trim=True)
plt.tight_layout()
plt.savefig('../Figures/RADseq_simulation-zygosity.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% plot the means of simulations vs means of our data
cm = plt.cm.get_cmap('Greys')
plt.figure(figsize=[13,1])
plt.subplot(131)
for seq in cer_means:
    plt.vlines(seq,color=cm(random.uniform(0.2,1)),ymin=0,ymax=1)
plt.subplot(132)
for seq in gen_means:
    plt.vlines(seq,color=cm(random.uniform(0.2,1)),ymin=0,ymax=1)
plt.subplot(133)
for seq in par_means:
    plt.vlines(seq,color=cm(random.uniform(0.2,1)),ymin=0,ymax=1)
cm = plt.cm.get_cmap('RdBu_r')
plt.subplot(131)
plt.vlines(cer_mean,color=cm(0.9),ymin=0,ymax=2)
plt.yticks([],[])
plt.xlabel('mean zygosity',weight='bold',fontsize=12)
plt.xticks([0.10,0.20,0.30,0.40])
plt.subplot(132)
cm = plt.cm.get_cmap('Greys')
plt.vlines(hetero_mean,color='dimgrey',ymin=0,ymax=2)
plt.yticks([],[])
plt.xticks([0.25,0.35,0.45,0.55,0.65,0.75])
plt.xlabel('mean zygosity',weight='bold',fontsize=12)
plt.subplot(133)
cm = plt.cm.get_cmap('RdBu_r')
plt.vlines(par_mean,color=cm(0.1),ymin=0,ymax=2)
plt.yticks([],[])
plt.xlabel('mean zygosity',weight='bold',fontsize=12)
plt.xticks([0.10,0.20,0.30,0.40])
sns.despine(trim=True,left=True)
#plt.tight_layout()
plt.savefig('../Figures/RADseq_simulation_hetero_mean.png',transparent=True,bbox_inches='tight',dpi=1000)

#%% calculates and prints the percent of simulations more extreme than each environment
hetero_mean={}
cer_mean={}
par_mean={}
for env in envs:
    hetero_mean[env]=(np.mean(data_hetero[env]))
    cer_mean[env]=(np.mean(data_cer[env]))
    par_mean[env]=(np.mean(data_par[env]))

for env in envs:
    print(env)
    cer=cer_mean[env]
    par=par_mean[env]
    hetero=hetero_mean[env]
    n=0
    for gen in gen_means:
        if gen<hetero:
            n+=1
    if n>5000:
        n=10000-n
    print ('hetero : ',round(((n)/10000)*100,2),'%')
    n=0
    for gen in cer_means:
        if gen<cer:
            n+=1
    if n>5000:
        n=10000-n
    print ('cer : ',round(((n)/10000)*100,2),'%')
    n=0
    for gen in par_means:
        if gen<par:
            n+=1
    if n>5000:
        n=10000-n
    print ('par : ',round(((n)/10000)*100,2),'%')


#%% prints simulation ranges
print('hetero',min(gen_means),'-',max(gen_means))
print('cer',min(cer_means),'-',max(cer_means))
print('par',min(par_means),'-',max(par_means))