#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 11:07:57 2019

@author: devin
"""

# =============================================================================
# to use the data from Kao et al. 2010 that they used for caryoscope analysis
# of F2 hybrids and determine the chromosome hybridity for each hybrid genome
# =============================================================================

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
from scipy import stats
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% Open and extract needed information from .gff files provided by Kao
genome_num=36
cutoff=-0.05
gen_chro_hybrid={}
for i in range(1,genome_num+1):
    gen_chro_hybrid[i]={}
    
chronum={1:'I',2:'II',3:'III',4:'IV',5:'V',6:'VI',7:'VII',8:'VIII',9:'IX',10:'X',11:'XI',12:'XII',13:'XIII',14:'XIV',15:'XV',16:'XVI'}
for chro in range(1,17):
    dataset=pd.read_csv("../DATA/Kao_DATA/{}_SC.gff".format(chro), delimiter='\t')
    data_dict={}
    data=dataset.to_dict('index')
    for i in range(1,genome_num+1):
        data_dict[i]=[]
    for gen in data:
        if data[gen]['chromosome']=='oligo':
            value=data[gen]['ID=chr{}'.format(chronum[chro])].split()
            value2=(value[4])
            value3=float(value2[1:-1])
            data_dict[data[gen]['1']].append(value3)
    
    total_num={}
    data_num={}
    for gen in data_dict:
        tot=0
        n=0
        for num in data_dict[gen]:
            tot+=1
            if num>=cutoff:
                n+=1
        data_num[gen]=n
        total_num[gen]=tot
    
    dataset=pd.read_csv("../DATA/Kao_DATA/{}_SP.gff".format(chro), delimiter='\t')
    data_dict={}
    data=dataset.to_dict('index')
    for i in range(1,genome_num+1):
        data_dict[i]=[]
    for gen in data:
        if data[gen]['chromosome']=='oligo':
            value=data[gen]['ID={}'.format(chro)].split()
            value2=(value[4])
            value3=float(value2[1:-1])
            data_dict[data[gen]['1']].append(value3)
    
    total_num2={}
    data_num2={}
    for gen in data_dict:
        tot=0
        n=0
        for num in data_dict[gen]:
            tot+=1
            if num>=cutoff:
                n+=1
        data_num2[gen]=n
        total_num2[gen]=tot
    diff=[]
    for gen in data_num:
        gen_chro_hybrid[gen][chro]=(data_num[gen]/(data_num[gen]+data_num2[gen]))#SP+SC
        #gen_chro_hybrid[gen][chro]=(data_num[gen]+(total_num2[gen]-data_num2[gen]))/(total_num[gen]+total_num2[gen])#SP+SC
        #gen_chro_hybrid[gen][chro]=(total_num2[gen]-data_num2[gen])/(total_num2[gen])#SP
        #gen_chro_hybrid[gen][chro]=(data_num[gen])/(total_num[gen])#SC

#%% Plot genome-wide hybridity

subset=list(range(1,37))
gen_hybridity=[]
for gen in gen_chro_hybrid:
    if gen in subset:
        x=list(gen_chro_hybrid[gen].values())
        gen_hybridity.append(np.mean(x))
print('mean : ',np.mean(gen_hybridity))
print('SEM  : ',stats.sem(gen_hybridity))
plt.figure(figsize=[2,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=16,zorder=0)
sns.boxplot(gen_hybridity,color='darkgrey',linewidth=2,saturation=0.75,orient='v')
plt.xticks([],[])
plt.scatter([0],np.mean(gen_hybridity),color='lightgray',marker='D',s=75,edgecolor='black',zorder=2)
plt.ylabel('genome hybridity',weight='bold',size=14)
plt.ylim(0.25,0.75)
plt.yticks([0.25,0.30,0.35,0.40,0.45,0.50,0.55,0.60,0.65,0.70,0.75])
sns.despine(trim=True, bottom=True)
plt.savefig('../Figures/Kao_genome_hybridity.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% Calculate and plot chromosome hybridity
chro_hybrid={}
for i in range(1,17):
    chro_hybrid[i]=[]
for gen in gen_chro_hybrid:
    if gen in subset:
        for i in range(1,17):
            chro_hybrid[i].append(gen_chro_hybrid[gen][i])
        
cmap = plt.cm.get_cmap('RdBu_r')
means={}
real_means={}
for chro in chro_hybrid:
    means[chro]=np.median(chro_hybrid[chro])
    real_means[chro]=np.mean(chro_hybrid[chro])
colors=[]
for seq in means:
    colors.append(cmap(means[seq]))

dataset=pd.DataFrame.from_dict(chro_hybrid)

plt.figure(2,figsize=[13,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=16,zorder=0)
sns.boxplot(data=dataset,palette=colors,linewidth=2,saturation=0.75)
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'],zorder=0)
plt.scatter([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],real_means.values(),color='lightgray',marker='D',s=75,edgecolor='black',zorder=2)
#plt.gca().get_legend().remove()
plt.xlabel('chromosome',weight='bold',size=14)
plt.ylabel('chromosome hybridity',weight='bold',size=14)
sns.despine(trim=True)
plt.savefig('../Figures/Kao_chromosome_hybridity.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% calculates delta chromosome hybridity (DCH)
corr={}
num=['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']
for x in num:
    for y in num:
        if float(x)>float(y):
            corr['chr{}_chr{}'.format(x,y)]=[]

for gen in gen_chro_hybrid:
    if gen in subset:
        for chro in gen_chro_hybrid[gen]:
            for chro2 in gen_chro_hybrid[gen]:
                if chro>chro2:
                    corr['chr{}_chr{}'.format(chro,chro2)].append(abs(gen_chro_hybrid[gen][chro]-gen_chro_hybrid[gen][chro2]))

corrfinal={}
for seq in corr:
    corrfinal[seq]=np.mean(corr[seq])
corr_SE={}
for seq in corr:
    corr_SE[seq]=stats.sem(corr[seq])
#%%
plt.figure(figsize=[4,2])
data = list(corrfinal.values())
cm = plt.cm.get_cmap('BrBG')
Y,X = np.histogram(data, 9)
x_span = 0.866
C = [cm(((x-0.071)/x_span)) for x in X]

plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
plt.xlabel('delta chromosome hybridity',weight='bold',fontsize=12)
plt.ylabel('interactions',weight='bold',fontsize=12)
#plt.yticks([0,5,10])
plt.xticks([0,0.2,0.4,0.6,0.8,1.0])
sns.despine(trim=True)
plt.savefig('../Figures/Kao_chr_correlation_histogram.png',bbox_inches='tight',dpi=1000,transparent=True)
print('DCH mean  : ',np.mean(list(corrfinal.values())))

#%% plot the heatmap of delta chromosome hybridity (DCH) for ALL ENVIRONMENTS
corr_fig=pd.DataFrame(index=num,columns=num)

for seq in corrfinal:
    z=seq.split('chr')[1]
    x=z[:-1]
    y=seq.split('chr')[2]
    corr_fig.at[x,y]=corrfinal[seq]
print('total DCH : ',sum(corrfinal.values()))
print('DCH mean  : ',np.mean(list(corrfinal.values())))
print('DCH median  : ',np.median(list(corrfinal.values())))
print('DCH max   : ',max(corrfinal.values()))
print('DCH min   : ',min(corrfinal.values()))

plt.figure(figsize=[12,10])
corr_fig.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(corr_fig,cmap='BrBG',linewidth=1,cbar_kws={"shrink":0.9},square=True,annot=True,vmax=0.937,vmin=0.071)
plt.xlabel('chromosome',weight='bold',fontsize=14)
plt.ylabel('chromosome',weight='bold',fontsize=14)
plt.yticks(rotation=0)
plt.savefig('../Figures/Kao_chr_correlation_heatmap.png',bbox_inches='tight',dpi=1000)

#%%
corr={}
num=['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']
for x in num:
    for y in num:
        if float(x)>float(y):
            corr['chr{}_chr{}_{}'.format(x,y,x)]=[]
            corr['chr{}_chr{}_{}'.format(x,y,y)]=[]
for gen in gen_chro_hybrid:
    for chro in gen_chro_hybrid[gen]:
        for chro2 in gen_chro_hybrid[gen]:
            if chro>chro2:
                corr['chr{}_chr{}_{}'.format(chro,chro2,chro)].append(gen_chro_hybrid[gen][chro])
                corr['chr{}_chr{}_{}'.format(chro,chro2,chro2)].append(gen_chro_hybrid[gen][chro2])

#%% calculates the Pearson correlation (r)
n=0
corrfinal={}
sig=[]
for seq in corr:
    for seq2 in corr:
        if seq!=seq2 and seq.split('_',1)[0]==seq2.split('_',1)[0] and seq.split('_',2)[1]==seq2.split('_',2)[1]:
            x,y=stats.pearsonr(corr[seq],corr[seq2])
            corrfinal['{}_{}'.format(seq2.split('_',2)[0],seq2.split('_',2)[1])]=x
            if y<0.05:
                    n+=1
            if y<0.01 and seq[:11] not in sig:
                sig.append(seq[:11])

for seq in corrfinal:
    corrfinal[seq]=round(corrfinal[seq],2)

#%% plot the heatmap of Pearson correlation between chromosomes
corr_fig=pd.DataFrame(index=num,columns=num)
for seq in corrfinal:
    z=seq.split('chr')[1]
    x=z[:-1]
    y=seq.split('chr')[2]
    corr_fig.at[x,y]=corrfinal[seq]
print('total corr : ',sum(corrfinal.values()))
print('corr mean  : ',np.mean(list(corrfinal.values())))
print('corr max   : ',max(corrfinal.values()))
print('corr min   : ',min(corrfinal.values()))

plt.figure(figsize=[12,10])
corr_fig.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(corr_fig,cmap='PRGn',linewidth=1,cbar_kws={"shrink":0.9},square=True,annot=True,annot_kws={"size": 11},vmax=0.6967,vmin=-0.681)
plt.xlabel('chromosome',weight='bold',fontsize=14)
plt.ylabel('chromosome',weight='bold',fontsize=14)
plt.yticks(rotation=0)
plt.savefig('../Figures/Kao_chr_correlation_heatmap_pearson-PuOr.png',bbox_inches='tight',dpi=1000)#,transparent=True)

#%% plot the distribution of Pearson correlation between chromosomes
plt.figure(figsize=[4,2])
data = list(corrfinal.values())
cm = plt.cm.get_cmap('PRGn')
Y,X = np.histogram(data, 15)
x_span = 1.377
C = [cm(((x+0.681)/x_span)) for x in X]
plt.vlines(np.mean(list(corrfinal.values())), color='black',linestyles='dashed',ymin=0,ymax=15)
plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
plt.xlabel('pearson correlation',weight='bold',fontsize=12)
plt.ylabel('interactions',weight='bold',fontsize=12)
plt.yticks([0,5,10,15])
plt.xticks([-0.75,0,0.75])
sns.despine(trim=True)
plt.savefig('../Figures/Kao_chr_corr_histogram_correlation.png',bbox_inches='tight',dpi=1000,transparent=True)
