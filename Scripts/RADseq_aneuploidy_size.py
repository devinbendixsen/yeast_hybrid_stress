#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 16:07:15 2019

@author: devin
"""
# =============================================================================
# Determine if there is a relationship between chromosome size and aneuploidy
# =============================================================================

#%% Import needed modules
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from scipy import stats

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% plot relationships for haploids and diploids
size={1:230218,2:813184,3:316620,4:1531933,5:576874,6:270161,7:1090940,8:562643,9:439888,10:745751,11:666816,12:1078177,13:924431,14:784333,15:1091291,16:948066}
haploid={1:0,2:16.9,3:11.3,4:52.8,5:1,6:0,7:0,8:0,9:7.5,10:11.3,11:13.2,12:24.5,13:45.2,14:35.8,15:7.5,16:18.9}
diploid={1:33.6,2:39,3:12.5,4:34.8,5:0,6:41.8,7:4.8,8:11.9,9:13.4,10:3.8,11:4.8,12:7.6,13:21.7,14:17.3,15:6.5,16:3.2}

plt.figure(1,figsize=[6,4])
for i in range(1,17):
    plt.figure(1,figsize=[6,4])
    plt.scatter(size[i],haploid[i],color='green')
    plt.figure(2,figsize=[6,4])
    plt.scatter(size[i],diploid[i],color='purple')