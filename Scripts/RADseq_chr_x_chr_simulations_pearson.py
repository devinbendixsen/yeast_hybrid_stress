#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 19:50:15 2019

@author: devin
"""
# =============================================================================
# Calculate and plot the pearson correlation of simulated chromosomes based
# on no chromosomal interactions (no epistasis)
# =============================================================================
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import random
import pandas as pd
from scipy import stats
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% creates all of the needed dictionaries and lists
sig_combos={}
for num in range(0,9):
    for num2 in range(0,9):
        sig_combos["{}_{}".format(num,num2)]=0

means=[]
cm = plt.cm.get_cmap('Greys')
pos_total_pos=[]
neg_total_pos=[]
pos_total_neg=[]
neg_total_neg=[]
pos_total_same=[]
neg_total_same=[]
pos_total=[]
neg_total=[]
for i in range(0,10000):
    data={}
    for j in range(0,16):
        for k in range(0,16):
            if j!=k and j>k:
                data["{}-{}_{}".format(j,k,j)]=[]
                data["{}-{}_{}".format(j,k,k)]=[]
    for i in range(0,24):
        x={}
        for j in range(0,16):
            x[j]=random.uniform(0,1)
        for j in x:
            for k in x:
                if j!=k and j>k:
                    data["{}-{}_{}".format(j,k,j)].append(x[j])
                    data["{}-{}_{}".format(j,k,k)].append(x[k])
    x=[]
    pos_sig=0
    neg_sig=0
    for l in data:
        for m in data:
            q=l.split('_',1)[0]
            r=m.split('_',1)[0]
            a=l.split('_',1)[1]
            b=m.split('_',1)[1]
            if q==r and l!=m and a>b:
                r,p=stats.pearsonr(data[l],data[m])
                x.append(r)
                if p<=0.01:
                    if r>0:
                        pos_sig+=1
                    else:
                        neg_sig+=1
    means.append(np.mean(x))
    pos_total.append(pos_sig)
    neg_total.append(neg_sig)
    if pos_sig>neg_sig:
        pos_total_pos.append(pos_sig)
        neg_total_pos.append(neg_sig)
    elif pos_sig<neg_sig:
        pos_total_neg.append(pos_sig)
        neg_total_neg.append(neg_sig)
    elif pos_sig==neg_sig:
        pos_total_same.append(pos_sig)
        neg_total_same.append(neg_sig)
    sig_combos["{}_{}".format(neg_sig,pos_sig)]+=1
    #sns.distplot(x,color='lightgrey',hist=False)
    #sns.kdeplot(x,color=cm(random.uniform(0.2,1)),bw=0.1)

plt.figure(figsize=[6,6])
plt.subplot(211)
cm = plt.cm.get_cmap('PRGn')
#plt.xlim(0,1)
plt.ylabel('interactions',weight='bold',fontsize=12)
#plt.xticks([0.0,0.2,0.4,0.6,0.8,1.0],[])
#plt.yticks([0,1,2,3,4,5,6,7])
plt.xlim(-1,1)
plt.subplot(212)
color={}
for env in data_envs:
    if np.mean(data_envs[env])>0:
        color[env]=cm(0.7)
    else:
        color[env]=cm(0.3)
    #sns.distplot(data_envs[env],color='brown',hist=False
    sns.kdeplot(data_envs[env],color=color[env],bw=0.1,shade=True)
plt.xlabel('pearson correlation',weight='bold',fontsize=12)
plt.ylabel('interactions',weight='bold',fontsize=12)
sns.despine(trim=True)
plt.xlim(-1,1)
plt.tight_layout()
plt.savefig('../Figures/RADseq_chr_correlation_Pearson_sim_dist.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%

cm = plt.cm.get_cmap('Greys')
plt.figure(figsize=[6,1])

for seq in means:
    plt.vlines(seq,color=cm(random.uniform(0.2,1)),ymin=0,ymax=1)
cm = plt.cm.get_cmap('PRGn')
plt.vlines([0.025,0.003,0.015,0.009,0.024,0.006,0.007,0.038],color=cm(0.7),ymin=0,ymax=2)
plt.vlines([-0.006,-0.005],color=cm(0.3),ymin=0,ymax=2)
plt.yticks([],[])
plt.xlabel('mean pearson correlation',weight='bold',fontsize=12)
plt.xticks([-0.10,-0.05,0.00,0.05,0.10])
sns.despine(trim=True,left=True)

plt.savefig('../Figures/RADseq_chr_correlation_sim_dist-mean.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
pos=[0,0,0,3,2,4,3,2,4,8]
neg=[0,0,1,1,3,0,5,2,3,3]
pos_prop={}
for i in range(0,9):
    pos_prop[i]=pos_total.count(i)/len(pos_total)
for i in pos_prop:
    plt.scatter(i,1,s=pos_prop[i]*2000)
#%%
data=pos_total
Y,X = np.histogram(data)

plt.bar(X[:-1],Y,width=X[1]-X[0],edgecolor='black',linewidth=1)
plt.vlines(pos, color='black',linestyles='dashed',ymin=0,ymax=15)
plt.xlabel('pearson correlation',weight='bold',fontsize=12)
plt.ylabel('interactions',weight='bold',fontsize=12)
#%%
import matplotlib.colors as colors
def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap
cmap = plt.get_cmap('Greys')
new_cmap = truncate_colormap(cmap, 0.01, 1)
#%%
sig_one={}
for num in range(0,9):
    for num2 in range(0,9):
        sig_one["{}_{}".format(num,num2)]=0
#%%
plt.figure(figsize=(6, 6))
num=[0,1,2,3,4,5,6,7,8]
heatmap_one=pd.DataFrame(index=num,columns=num)
heatmap_one.fillna(value=np.nan, inplace=True)
for seq in sig_one:
        x=float(seq[0])
        y=float(seq[2])
        if sig_one[seq]>0:
            heatmap_one.at[x,y]=(sig_one[seq])
        else:
            heatmap_one.at[x,y]=(sig_one[seq])
#%%

sns.heatmap(heatmap_one,color='lightgrey',linewidth=1,cbar=False,square=True)
plt.xlim(0,9)
plt.ylim(0,9)
sns.despine(trim=True,offset=5)
#%%
plt.figure(figsize=(6, 6))
cm = plt.cm.get_cmap('PRGn')
num=[0,1,2,3,4,5,6,7,8]
#plt.figure(figsize=(6, 6))
heatmap_fig=pd.DataFrame(index=num,columns=num)
heatmap_fig.fillna(value=np.nan, inplace=True)

for seq in sig_combos:
        x=float(seq[0])
        y=float(seq[2])
        if sig_combos[seq]>0:
            heatmap_fig.at[x,y]=(sig_combos[seq])
        else:
            heatmap_fig.at[x,y]=(sig_combos[seq])
pos1=[3,4,4,8]
neg1=[1,0,3,3]
pos2=[0,2,3]
neg2=[1,3,5]
pos3=[0,0,2]
neg3=[0,0,2]


#sns.heatmap(heatmap_fig,cmap='bone_r',linewidth=1,cbar=False,square=True)
sns.kdeplot(neg_total,pos_total,cmap='Greys',shade=True,bw=0.4,shade_lowest=False,n_levels=10)
plt.scatter(neg3,pos3,color='dimgrey',s=100,marker='s',edgecolor='black',linewidth=2)
plt.scatter(neg1,pos1,color=cm(0.8),s=100,marker='s',edgecolor='black',linewidth=2)
plt.scatter(neg2,pos2,color=cm(0.2),s=100,marker='s',edgecolor='black',linewidth=2)
#plt.scatter(neg_total,pos_total,s=1,color='grey')
plt.xlim(-0.5,8.5)
plt.ylim(-0.5,8.5)
sns.despine(trim=True,offset=5)
plt.xlabel('n negative correlations',weight='bold',fontsize=12)
plt.ylabel('n positive correlations',weight='bold',fontsize=12)
plt.savefig('../Figures/RADseq_pearson_simulation_scatter.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
cm = plt.cm.get_cmap('PRGn')
f, ax = plt.subplots(figsize=(6, 6))
#sns.kdeplot(pos_total,neg_total,bw=0.4,shade=True,cmap='Greys')
pos1=[3,5,7,8,11,16]
neg1=[2,4,5,7,7,10]
pos2=[1,2,5,5]
neg2=[3,4,6,8]
pos3=[]
neg3=[]
sns.kdeplot(neg_total,pos_total,cmap='Greys',shade=True,bw=0.4,shade_lowest=False)
plt.scatter(neg3,pos3,color='grey',s=50,marker='s',edgecolor='black')

plt.scatter(neg1,pos1,color=cm(0.8),s=50,marker='s',edgecolor='black')
plt.scatter(neg2,pos2,color=cm(0.2),s=50,marker='s',edgecolor='black')
plt.scatter(neg_total,pos_total,s=1,color='grey')
plt.xlim(-.5,16.5)
plt.ylim(-.5,16.5)
sns.despine(trim=True)
plt.xlabel('n negative correlations',weight='bold',fontsize=12)
plt.ylabel('n positive correlations',weight='bold',fontsize=12)
plt.savefig('../Figures/RADseq_pearson_simulation_scatter0.05.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%

cm = plt.cm.get_cmap('Greys')
plt.figure(figsize=[6,6])
plt.subplot(311)
sns.distplot(pos_total,color='dimgrey')
cm = plt.cm.get_cmap('PRGn')
plt.vlines(pos,color=cm(0.7),ymin=0,ymax=0.1)
plt.xlabel('n positive correlations',weight='bold',fontsize=12)

plt.subplot(312)
sns.distplot(pos_total,color='dimgrey')
cm = plt.cm.get_cmap('PRGn')
plt.vlines(neg,color=cm(0.7),ymin=0,ymax=0.2)
plt.xlabel('n negative correlations',weight='bold',fontsize=12)
sns.despine(trim=True)
plt.tight_layout()
plt.savefig('../Figures/RADseq_chr_correlation_sim_sig-corr.png',bbox_inches='tight',dpi=1000,transparent=True)