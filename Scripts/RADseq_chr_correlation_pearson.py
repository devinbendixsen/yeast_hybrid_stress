#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 15:23:34 2019

@author: devin
"""
# =============================================================================
# Code to plot the Pearson correlation of chromosome hybridity between chromosomes
# =============================================================================
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from scipy import stats
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% import data supplied by Zebin Zhang from sequencing data
dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")

#%% creates all of the needed dictionaries and lists
corr={}
num=['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16']
for x in num:
    for y in num:
        if float(x)>float(y):
            corr['chr{}_chr{}_{}'.format(x,y,x)]=[]
            corr['chr{}_chr{}_{}'.format(x,y,y)]=[]

samples=[]
for sample in dataset.Sample:
    if sample not in samples:
        samples.append(sample)

chronum=[]
for sample in dataset.Chromosome:
    if sample not in chronum:
        chronum.append(sample)

sam_chro={}
for sam in samples:
    for chro in chronum:
        sam_chro['{}_{}'.format(sam,chro)]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']==chro),'P_cerevisiae']

data={}
for seq in sam_chro:
    data[seq]=sam_chro[seq].item()

#%% calculates the Pearson correlation (r) across ALL ENVIRONMENTS
for seq in data:
    for seq2 in data:
        if seq[:-6]==seq2[:-6] and (float(seq[-2:])>float(seq2[-2:])):
            corr['chr{}_chr{}_{}'.format(seq[-2:],seq2[-2:],seq[-2:])].append(data[seq])
            corr['chr{}_chr{}_{}'.format(seq[-2:],seq2[-2:],seq2[-2:])].append(data[seq2])

n=0
corrfinal={}
sig=[]
for seq in corr:
    for seq2 in corr:
        if seq[:11]==seq2[:11] and seq!=seq2:
            x,y=stats.pearsonr(corr[seq],corr[seq2])
            corrfinal[seq[:11]]=x
            if y<0.05:
                    n+=1
            if y<0.01 and seq[:11] not in sig:
                sig.append(seq[:11])

for seq in corrfinal:
    corrfinal[seq]=round(corrfinal[seq],2)

#%% plot the heatmap of Pearson correlation between chromosomes across ALL ENVIRONMENTS
corr_fig=pd.DataFrame(index=num,columns=num)

for seq in corrfinal:
    x=seq[3:5]
    y=seq[-2:]
    corr_fig.at[x,y]=corrfinal[seq]
print('total corr : ',sum(corrfinal.values()))
print('corr mean  : ',np.mean(list(corrfinal.values())))
print('corr max   : ',max(corrfinal.values()))
print('corr min   : ',min(corrfinal.values()))

plt.figure(figsize=[12,10])
corr_fig.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(corr_fig,cmap='PRGn',linewidth=1,cbar_kws={"shrink":0.9},square=True,annot=True,annot_kws={"size": 11},vmax=0.6967,vmin=-0.681)
plt.xlabel('chromosome',weight='bold',fontsize=14)
plt.ylabel('chromosome',weight='bold',fontsize=14)
plt.yticks(rotation=0)
plt.savefig('../Figures/RADseq_chr_correlation_heatmap_pearson-PuOr.png',bbox_inches='tight',dpi=1000)#,transparent=True)

#%% plot the distribution of Pearson correlation between chromosomes across ALL ENVIRONMENTS
plt.figure(figsize=[4,2])
data = list(corrfinal.values())
cm = plt.cm.get_cmap('PRGn')
Y,X = np.histogram(data, 15)
x_span = 1.377
C = [cm(((x+0.681)/x_span)) for x in X]
plt.vlines(np.mean(list(corrfinal.values())), color='black',linestyles='dashed',ymin=0,ymax=15)
plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
plt.xlabel('pearson correlation',weight='bold',fontsize=12)
plt.ylabel('interactions',weight='bold',fontsize=12)
plt.yticks([0,5,10,15])
plt.xticks([-0.75,0,0.75])
sns.despine(trim=True)
plt.savefig('../Figures/RADseq_chr_corr_histogram_correlation.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% create function to plot the heatmap of Pearosn correlation for each environment
def enviroplot(Environment):
    dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")
    enviroslice=dataset['Environment']==Environment
    dataset=dataset[enviroslice]
    print(Environment)
    corr={}
    num=['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16']
    for x in num:
        for y in num:
            if float(x)>float(y):
                corr['chr{}_chr{}_{}'.format(x,y,x)]=[]
                corr['chr{}_chr{}_{}'.format(x,y,y)]=[]
    
    samples=[]
    for sample in dataset.Sample:
        if sample not in samples:
            samples.append(sample)
    
    chronum=[]
    for sample in dataset.Chromosome:
        if sample not in chronum:
            chronum.append(sample)
    sam_chro={}
    for sam in samples:
        for chro in chronum:
            sam_chro['{}_{}'.format(sam,chro)]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']==chro),'P_cerevisiae']
    
    data={}
    for seq in sam_chro:
        data[seq]=sam_chro[seq].item()
    
    for seq in data:
        for seq2 in data:
            if seq[:-6]==seq2[:-6] and (float(seq[-2:])>float(seq2[-2:])):
                corr['chr{}_chr{}_{}'.format(seq[-2:],seq2[-2:],seq[-2:])].append(data[seq])
                corr['chr{}_chr{}_{}'.format(seq[-2:],seq2[-2:],seq2[-2:])].append(data[seq2])
    
    sig=[]
    neg_sig=[]
    pos_sig=[]
    n=0
    corrfinal={}
    for seq in corr:
        for seq2 in corr:
            if seq[:11]==seq2[:11] and seq!=seq2:
                x,y=stats.pearsonr(corr[seq],corr[seq2])
                corrfinal[seq[:11]]=x
                if y<0.05:
                    n+=1
                if y<0.05 and seq[:11] not in sig:
                    sig.append(seq[:11])
                    if x>=0:
                        pos_sig.append(seq[:11])
                    elif x<=0:
                        neg_sig.append(seq[:11])
    print(Environment)
    print(sig)
    print(sum(corrfinal.values()))
    print(np.mean(list(corrfinal.values())))
    print('max :',max(corrfinal.values()))
    print('min :',min(corrfinal.values()))
    print('sig :', len(sig))
    print('neg sig :', len(neg_sig))
    print('pos sig :', len(pos_sig))
    
    plt.figure(figsize=[4,2])
    data = list(corrfinal.values())
    cm = plt.cm.get_cmap('PRGn')
    Y,X = np.histogram(data, 20)
    x_span = 1.377
    C = [cm(((x+0.681)/x_span)) for x in X]
    
    plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
    plt.vlines(np.mean(list(corrfinal.values())), color='black',linestyles='dashed',ymin=0,ymax=15)
    plt.xlabel('pearson correlation',weight='bold',fontsize=12)
    plt.ylabel('interactions',weight='bold',fontsize=12)
    plt.yticks([0,5,10,15])
    plt.xticks([-0.75,0,0.75])
    sns.despine(trim=True)
    plt.savefig('../Figures/RADseq_chr_corr_{}_histogram_correlation.png'.format(Environment),bbox_inches='tight',dpi=1000,transparent=True)
    print(stats.normaltest(data))
    
    corr_fig=pd.DataFrame(index=num,columns=num)
    for seq in corrfinal:
        x=seq[3:5]
        y=seq[-2:]
        corr_fig.at[x,y]=corrfinal[seq]

    plt.figure(figsize=[8,8])
    corr_fig.fillna(value=np.nan, inplace=True)
    sns.heatmap(corr_fig,cmap='PRGn',linewidth=1,cbar=False,cbar_kws={"shrink":0.9},square=True,vmax=0.6967,vmin=-0.681)
    plt.xlabel('chromosome',weight='bold',fontsize=14)
    plt.ylabel('chromosome',weight='bold',fontsize=14)
    plt.yticks(rotation=0)
    #plt.title('{}'.format(Environment),weight='bold',fontsize=15)
    plt.savefig('../Figures/RADseq_{}_heatmap_correlation.png'.format(Environment),bbox_inches='tight',dpi=1000,transparent=True)
    return corr

#%% call enviroplot function
envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
for env in envs:
    enviroplot(env)
    
#%% plot individual correlations
corr=enviroplot('Caffeine')

plt.figure(figsize=[4,4])
cmap = plt.cm.get_cmap('PRGn')
xlist=corr['chr13_chr11_13']
ylist=corr['chr13_chr11_11']
colors=cmap(0.0)
datas=pd.DataFrame({'x':xlist,'y':ylist})
g=sns.JointGrid(x='x',y='y',data=datas)
g = g.plot_joint(plt.scatter, color=colors,edgecolor="black",s=100,linewidth=2,marker='h')
g = g.plot_marginals(sns.distplot, color=colors)
g = g.plot_joint(sns.regplot, color=colors)
plt.xlabel('chromosome 13 hybridity',weight='bold',fontsize=15)
plt.ylabel('chromosome 11 hybridity',weight='bold',fontsize=15)
plt.xlim(0,1)
plt.ylim(0,1)
sns.despine(trim=True)
plt.savefig('../Figures/RADseq_13-11_correlation.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% plot individual correlations
corr=enviroplot('Caffeine')
plt.figure(figsize=[4,4])
cmap = plt.cm.get_cmap('PRGn')
xlist=corr['chr13_chr01_13']
ylist=corr['chr13_chr01_01']
colors=cmap(1.0)
datas=pd.DataFrame({'x':xlist,'y':ylist})
g=sns.JointGrid(x='x',y='y',data=datas)
g = g.plot_joint(plt.scatter, color=colors,edgecolor="black",s=100,linewidth=2,marker='h')
g = g.plot_marginals(sns.distplot, color=colors)
g = g.plot_joint(sns.regplot, color=colors)
plt.xlabel('chromosome 13 hybridity',weight='bold',fontsize=15)
plt.ylabel('chromosome 1 hybridity',weight='bold',fontsize=15)
plt.xlim(0,1)
plt.ylim(0,1)
sns.despine(trim=True)
plt.savefig('../Figures/RADseq_13-01_correlation.png',bbox_inches='tight',dpi=1000,transparent=True)