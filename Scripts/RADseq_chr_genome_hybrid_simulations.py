#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 14:44:25 2019

@author: devin
"""
# =============================================================================
# Calculate and plot the genome-wide hybridity of simulated chromosomes based
# on no chromosomal interactions (no epistasis)
# =============================================================================
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import random
import pandas as pd
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% import and calculate the distribution of genome-wide hybridity
def enviroplot(Environment):
    dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")
    enviroslice=dataset['Environment']==Environment
    dataset=dataset[enviroslice]
    
    corr={}
    num=['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16']
    for x in num:
        for y in num:
            if float(x)>float(y):
                corr['chr{}_chr{}'.format(x,y)]=[]
    
    samples=[]
    for sample in dataset.Sample:
        if sample not in samples:
            samples.append(sample)
    
    chronum=[]
    for sample in dataset.Chromosome:
        if sample not in chronum:
            chronum.append(sample)
    
    sam_hybrid={}
    sam_mean=[]
    for sam in samples:
        sam_hybrid[sam]=[]
        for chro in chronum:
            x=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']==chro),'P_cerevisiae']
            sam_hybrid[sam].append(float(x))
        sam_mean.append(np.mean(list(sam_hybrid.values())))
    
    return sam_mean

envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
data_envs={}
for env in envs:
    data_envs[env]=enviroplot(env)

#%% calculate and plot the distribution of genome-wide hybridity of simualted chromosomes
#   compare and plot against the distribution of genome-wide hybridity in our data
cm = plt.cm.get_cmap('Greys')
plt.figure(figsize=[6,6])
plt.subplot(211)
colors={}
gen_means=[]
for i in range(0,10000):
    genome=[]
    chromtotal=[]
    for i in range(0,24):
        x={}
        chrom=[]
        for j in range(0,16):
            x[j]=random.uniform(0,1)
            chrom.append(x[j])
            chromtotal.append(x[j])
        genome.append(np.mean(list(x.values())))
    sns.kdeplot(genome,color=cm(random.uniform(0.2,1)),bw=0.04)
    gen_means.append(np.mean(genome))
plt.xlim(0,1)
plt.yticks([0,2,4,6,8])
plt.ylabel('genomes',weight='bold',fontsize=12)
plt.subplot(212)
cm = plt.cm.get_cmap('RdBu')
colors={}
envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
data_envs={}
for env in envs:
    data_envs[env]=enviroplot(env)
for env in data_envs:
    print(env,np.mean(data_envs[env]))
    if np.mean(data_envs[env])<0.49:
        colors[env]=cm(.8)
    else:
        colors[env]=cm(0.2)
    sns.kdeplot(data_envs[env],shade=True,color=colors[env],bw=0.04)
plt.xlim(0,1)
plt.xlabel('genome hybridity',weight='bold',fontsize=12)
plt.ylabel('genomes',weight='bold',fontsize=12)
sns.despine(trim=True)
plt.tight_layout()
plt.savefig('../Figures/RADseq_pearson_simulation_genome-hybridity.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% plot the means of simulations vs means of our data
cm = plt.cm.get_cmap('Greys')
plt.figure(figsize=[6,1])
env_means=[0.51,0.505,0.532,0.54,0.528,0.545,0.526,0.55,0.508]
for seq in gen_means:
    plt.vlines(seq,color=cm(random.uniform(0.2,1)),ymin=0,ymax=1)
cm = plt.cm.get_cmap('RdBu')
plt.vlines(env_means,color=cm(0.2),ymin=0,ymax=2)
plt.vlines([0.49],color=cm(0.8),ymin=0,ymax=2)
plt.yticks([],[])
plt.xlabel('mean genome hybridity',weight='bold',fontsize=12)
plt.xticks([0.44,0.46,0.48,0.50,0.52,0.54,0.56])
sns.despine(trim=True,left=True)
plt.savefig('../Figures/RADseq_chr_correlation_sim_dist-mean-genome-hybridity.png',transparent=True,bbox_inches='tight',dpi=1000)
#%% print range
print(min(gen_means),'-',max(gen_means))