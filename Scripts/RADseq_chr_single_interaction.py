#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 12:04:56 2019

@author: devin
"""
# =============================================================================
# Plot the delta chromosome hybridity (DCH) for a single chromosome pair across all environments
# =============================================================================
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from scipy import stats

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% import data supplied by Zebin Zhang from sequencing data
dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")

#%% create function to plot all environments as heatmap for a single interaction
def chr_interaction(chrX,chrY):
    num=(chrX,chrY)
    env_chr={}
    envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
    for env in envs:
        env_chr['{}'.format(env)]=[]
    
    samples=[]
    for sample in dataset.Sample:
        if sample not in samples:
            samples.append(sample)
    
    samples=[]
    for sample in dataset.Sample:
        if sample not in samples:
            samples.append(sample)
    
    sam_chro={}
    for sam in samples:
        for chro in num:
            sam_chro['{}_{}'.format(sam,chro)]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']==chro),'P_cerevisiae']
    data={}
    for seq in sam_chro:
        data[seq]=sam_chro[seq].item()
    
    sam_env={}
    for sam in samples:
            sam_env[sam]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']=='chr01') ,'Environment']
    data_env={}
    for seq in sam_env:
        data_env[seq]=sam_env[seq].item()
    
    for seq in data:
        for seq2 in data:
            if seq[:-6]==seq2[:-6] and seq[-5:]==chrX and seq2[-5:]==chrY:
                env_chr[data_env[seq[:-6]]].append(abs(data[seq]-data[seq2]))
    
    interaction=['{}-{}'.format(chrX,chrY)]
    env_chr_fig=pd.DataFrame(index=envs,columns=interaction)
    env_chr_mean={}
    for seq in env_chr:
        env_chr_mean[seq]=np.mean((env_chr[seq]))
    
    for env in envs:
        y=interaction
        x=env
        env_chr_fig.at[x,y]=env_chr_mean[env]
    
    plt.figure(figsize=[10,4])
    env_chr_fig.fillna(value=np.nan, inplace=True)
    sns.heatmap(env_chr_fig,cmap='BrBG',linewidth=2,linecolor='black',cbar_kws={"shrink":0.8},square=True,annot=False,robust=True,vmax=0.937,vmin=0.071)
    #plt.xlabel('chromosome',weight='bold',fontsize=14)
    plt.ylabel('environment',weight='bold',fontsize=14)
    #plt.savefig('../Figures/RADseq_env_chr_hybridity_heatmap.png',bbox_inches='tight',dpi=1000)#,transparent=True)

    F, p = stats.f_oneway(env_chr['Caffeine'], env_chr['Citric_Acid'], env_chr['DMSO'], env_chr['EtOH'], env_chr['Hydrogen_Peroxide'], env_chr['Lithium_Acetate'], env_chr['NaCl'], env_chr['Nipagin'], env_chr['Salicylic_Acid'], env_chr['ZnSO4'])
    return(F, p)

#%% plot the chromosome pair interaction across all environments
chrX='chr07'
chrY='chr04'
F,p=chr_interaction(chrX,chrY)
print('F-value : ',F,'p-value : ',p)
