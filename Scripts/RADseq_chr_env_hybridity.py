#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 11:40:53 2019

@author: devin
"""
# =============================================================================
# Code to determine and plot the chromosome hybridity of S. cer and S. par 
# hybrids in ten different environments
# =============================================================================

#%% Import needed modules
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from scipy import stats

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% import data supplied by Zebin Zhang from sequencing data
#dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")
dataset=pd.read_csv("../DATA/GLMM_F2Hybrids_Aneuploidy_Chromosome.txt",delimiter="\t")
#%% creates a dictionary full of the combinations chr and environment
env_chr={}
num=['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16']
envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
for x in num:
    for y in envs:
        env_chr['{}_chr{}'.format(y,x)]=[]

#%% creates needed dictionaries and lists for samples and calculates the mean chromosome hybridity
samples=[]
for sample in dataset.Sample:
    if sample not in samples:
        samples.append(sample)
        
chronum=[]
for sample in dataset.Chromosome:
    if sample not in chronum:
        chronum.append(sample)
        
sam_chro={}
for sam in samples:
    for chro in chronum:
        sam_chro['{}_{}'.format(sam,chro)]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']==chro),'P_cerevisiae']
data={}
for seq in sam_chro:
    data[seq]=sam_chro[seq].item()
    
sam_env={}
for sam in samples:
        sam_env[sam]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']=='chr01') ,'Environment']

data_env={}
for seq in sam_env:
    data_env[seq]=sam_env[seq].item()

for seq in data:
    env_chr['{}_{}'.format(data_env[seq[:-6]],seq[-5:])].append(data[seq])

env_chr_mean={}
for seq in env_chr:
    env_chr_mean[seq]=np.mean(env_chr[seq])

#%% Generates blank table for heatmap
env_chr_fig=pd.DataFrame(index=envs,columns=num)

#%% Fills table for heatmap with the correct environment and chromosome
for seq in env_chr_mean:
    y=seq[-2:]
    x=seq[:-6]
    env_chr_fig.at[x,y]=env_chr_mean[seq]

#%% Plots heatmap depicting chromosome hybridity for each chromosome and environment
plt.figure(1, figsize=[16,10])
env_chr_fig.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(env_chr_fig,cmap='RdBu_r',linewidth=1,cbar_kws={"shrink":0.8},square=True,annot=True,robust=True,vmin=0.1,vmax=0.9)
plt.xlabel('chromosome',weight='bold',fontsize=14)
plt.ylabel('environment',weight='bold',fontsize=14)
plt.savefig('../Figures/RADseq_env_chr_hybridity_heatmap.png',bbox_inches='tight',dpi=1000)#,transparent=True)

#%% Determines and plots the distribution (barplot) of chromosome hybridity for each chromosome across ALL ENVIRONMENTS
cmap = plt.cm.get_cmap('RdBu_r')
means=dataset.groupby(['Chromosome'])['P_cerevisiae'].median().values
real_means=dataset.groupby(['Chromosome'])['P_cerevisiae'].mean().values
colors=[]
for seq in means:
    colors.append(cmap(seq))

plt.figure(2,figsize=[13,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=16,zorder=0)
sns.boxplot(x='Chromosome',y='P_cerevisiae',palette=colors,data=dataset,linewidth=2,saturation=0.75)
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'],zorder=0)
plt.scatter([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],real_means,color='lightgray',marker='D',s=75,edgecolor='black',zorder=2)
#plt.gca().get_legend().remove()
plt.xlabel('chromosome',weight='bold',size=14)
plt.ylabel('chromosome hybridity',weight='bold',size=14)
sns.despine(trim=True)
plt.savefig('../Figures/chromosome_hybridity.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% Determines if there is a significant difference for each chromosome across the environments
chro='chr11'
for chro in chronum:
    data_hybrid={}
    for seq in env_chr:
        if seq[-5:] == chro:
            data_hybrid[seq[:-6]]=env_chr[seq]
    F, p = stats.f_oneway(data_hybrid['Caffeine'], data_hybrid['Citric_Acid'], data_hybrid['DMSO'], data_hybrid['EtOH'], data_hybrid['Hydrogen_Peroxide'], data_hybrid['Lithium_Acetate'], data_hybrid['NaCl'], data_hybrid['Nipagin'], data_hybrid['Salicylic_Acid'], data_hybrid['ZnSO4'])
    print('Chr:', chro, 'F-value:',round(F,3),'p-value:',round(p,4))
