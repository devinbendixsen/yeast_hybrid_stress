#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 19:50:15 2019

@author: devin
"""
# =============================================================================
# Calculate and plot the delta chromosome hybridity (DCH) of simulated chromosomes based
# on no chromosomal interactions (no epistasis)
# =============================================================================
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import random
import pandas as pd
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% import and calculate the distribution of delta chromosome hybridity (DCH)
def enviroplot(Environment):
    dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")
    enviroslice=dataset['Environment']==Environment
    dataset=dataset[enviroslice]
    
    corr={}
    num=['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16']
    for x in num:
        for y in num:
            if float(x)>float(y):
                corr['chr{}_chr{}'.format(x,y)]=[]
                
    samples=[]
    for sample in dataset.Sample:
        if sample not in samples:
            samples.append(sample)
            
    chronum=[]
    for sample in dataset.Chromosome:
        if sample not in chronum:
            chronum.append(sample)
    sam_chro={}
    for sam in samples:
        for chro in chronum:
            sam_chro['{}_{}'.format(sam,chro)]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']==chro),'P_cerevisiae']
    
    data={}
    for seq in sam_chro:
        data[seq]=sam_chro[seq].item()
    
    for seq in data:
        for seq2 in data:
            if seq[:-6]==seq2[:-6] and (float(seq[-2:])>float(seq2[-2:])):
                corr['chr{}_chr{}'.format(seq[-2:],seq2[-2:])].append(abs(data[seq]-data[seq2]))
    
    corrfinal={}
    for seq in corr:
        corrfinal[seq]=np.mean(corr[seq])
    data = list(corrfinal.values())
    return data

envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
data_envs={}
for env in envs:
    data_envs[env]=enviroplot(env)
#%% calculate and plot the distribution of delta chromosome hybridity of simualted chromosomes
#   compare and plot against the distribution of delta chromosome hybridity in our data
plt.figure(figsize=[6,6])
plt.subplot(211)
means=[]
cm = plt.cm.get_cmap('Greys')
for i in range(0,10000):
    data={}
    for j in range(0,16):
        for k in range(0,16):
            if j!=k and j>k:
                data["{}_{}".format(j,k)]=[]
    for i in range(0,24):
        x={}
        for j in range(0,16):
            x[j]=random.uniform(0,1)
        for j in x:
            for k in x:
                if j!=k and j>k:
                    data["{}_{}".format(j,k)].append((abs(x[j]-x[k])))
        
    x=[]
    for l in data:
        x.append(np.mean(data[l]))
    means.append(np.mean(x))
    sns.kdeplot(x,color=cm(random.uniform(0.2,1)),bw=0.04)
cm = plt.cm.get_cmap('BrBG')
plt.xlim(0,0.9)
plt.ylabel('interactions',weight='bold',fontsize=12)
plt.yticks([0,1,2,3,4,5,6,7])
plt.subplot(212)

color={}
for env in data_envs:
    if np.mean(data_envs[env])>0.364:
        color[env]=cm(0.4)
    elif np.mean(data_envs[env])>0.3:
        color[env]=cm(0.25)
    else:
        color[env]=cm(0.1)
    sns.kdeplot(data_envs[env],color=color[env],shade=True,bw=0.04)
plt.xlim(0,0.9)
plt.yticks([0,1,2,3,4,5])
plt.xlabel('delta chromosome hybridity',weight='bold',fontsize=12)
plt.ylabel('interactions',weight='bold',fontsize=12)
sns.despine(trim=True)
plt.tight_layout()
plt.savefig('../Figures/RADseq_chr_correlation_sim_dist.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% plot the means of simulations vs means of our data
cm = plt.cm.get_cmap('Greys')
plt.figure(figsize=[6,1])
for seq in means:
    plt.vlines(seq,color=cm(random.uniform(0.2,1)),ymin=0,ymax=1)
cm = plt.cm.get_cmap('BrBG')
plt.vlines([0.3889,0.3711],color=cm(0.4),ymin=0,ymax=2)
plt.vlines([0.357,0.354,0.339,0.336],color=cm(0.25),ymin=0,ymax=2)
plt.vlines([0.239,0.2207,0.2215,0.216],color=cm(0.1),ymin=0,ymax=2)
plt.yticks([],[])
plt.xlabel('mean delta chromosome hybridity',weight='bold',fontsize=12)
plt.xticks([0.20,0.25,0.3,0.35,0.4])
sns.despine(trim=True,left=True)
plt.savefig('../Figures/RADseq_chr_correlation_sim_dist-mean-dch.png',transparent=True,bbox_inches='tight',dpi=1000)

#%%
print(min(means),'-',max(means))
print(np.mean(means))