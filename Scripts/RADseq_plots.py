 # -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)
#%%
dataset=pd.read_csv("../DATA/GLMM_genome.txt",delimiter="\t")

plt.figure(figsize=[16,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=10,zorder=0)
sns.violinplot(x='Environment',y='P_cerevisiae',palette='RdBu',data=dataset,scale='width',inner='quartile',linewidth=2,bw=0.2,split=True,legend=False)
sns.despine(trim=True)
plt.gca().get_legend().remove()
plt.xticks([0,1,2,3,4,5,6,7,8,9],[])
plt.xlabel('')
plt.ylabel('')
#plt.savefig('../Figures/Environment_genome.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")

plt.figure(figsize=[16,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=10,zorder=0)
sns.violinplot(x='Environment',y='P_cerevisiae',palette='RdBu',data=dataset,scale='area',inner='quartile',linewidth=2,bw=0.2,hue='Top_Bottom',split=True,legend=False,cut=0)
#sns.boxplot(x='Environment',y='P_cerevisiae',palette=['indianred','lightgrey'],data=dataset,linewidth=2,hue='Top.Bottom',saturation=0.75)
sns.despine(trim=True)
plt.gca().get_legend().remove()
plt.xticks([0,1,2,3,4,5,6,7,8,9],[])
plt.xlabel('')
plt.ylabel('')
plt.savefig('../Figures/Environment_chromosome.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
means=dataset.groupby(['Chromosome'])['P_cerevisiae'].mean().values

plt.figure(figsize=[16,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=16,zorder=0)
sns.boxplot(x='Chromosome',y='P_cerevisiae',palette=['indianred','lightgrey'],data=dataset,linewidth=2,saturation=0.75)
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])
#plt.gca().get_legend().remove()
plt.xlabel('chromosome',weight='bold',size=14)
plt.ylabel('hybridity',weight='bold',size=14)
sns.despine(trim=True)
plt.savefig('../Figures/chromosome_hybridity.png',bbox_inches='tight',dpi=1000,transparent=True)




#%%
from scipy import stats
envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
dataset=pd.read_csv("../DATA/GLMM_genome.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')]
    x,y=stats.ttest_ind(top.P_cerevisiae, bottom.P_cerevisiae)
    print(env, 't-stat :', x, 'p-value :', y)
    
#%%

dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')]
    x,y=stats.ttest_ind(top.P_cerevisiae, bottom.P_cerevisiae)
    print(env, 't-stat :', x, 'p-value :', y)

#%%
dataset=pd.read_csv("../DATA/GLMM_bin_10k_clean.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')]
    x,y=stats.ttest_ind(top.S_cerevisiae_S288c, bottom.S_cerevisiae_S288c)
    print(env, 't-stat :', x, 'p-value :', y)

#%%
dataset=pd.read_csv("../DATA/GLMM_bin_10k_clean.txt",delimiter="\t")

plt.figure(figsize=[16,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=10,zorder=0)
sns.violinplot(x='Environment',y='S_cerevisiae_S288c',palette='RdBu',data=dataset,scale='count',inner='quartile',linewidth=2,bw=0.2,hue='Top_Bottom',split=True,legend=False,cut=0)
sns.despine(trim=True)
plt.gca().get_legend().remove()
plt.xticks([0,1,2,3,4,5,6,7,8,9],[])
plt.xlabel('')
plt.ylabel('')
plt.savefig('../Figures/Environment_10kbin.png',bbox_inches='tight',dpi=1000,transparent=True)


#%% Top vs Bottom analysis within an environment with only haploids
from scipy import stats
envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
dataset=pd.read_csv("../DATA/GLMM_genome.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')&(dataset.Ploidy=='Haploidy')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')&(dataset.Ploidy=='Haploidy')]
    x,y=stats.ttest_ind(top.P_cerevisiae, bottom.P_cerevisiae)
    print(env, 't-stat :', x, 'p-value :', y)
    

dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')&(dataset.Ploidy=='Haploidy')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')&(dataset.Ploidy=='Haploidy')]
    x,y=stats.ttest_ind(top.P_cerevisiae, bottom.P_cerevisiae)
    print(env, 't-stat :', x, 'p-value :', y)

dataset=pd.read_csv("../DATA/GLMM_bin_10k_clean.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')&(dataset.Ploidy=='Haploidy')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')&(dataset.Ploidy=='Haploidy')]
    x,y=stats.ttest_ind(top.S_cerevisiae_S288c, bottom.S_cerevisiae_S288c)
    print(env, 't-stat :', x, 'p-value :', y)
    
#%% Top vs Bottom analysis within an environment with only diploids
from scipy import stats
envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
dataset=pd.read_csv("../DATA/GLMM_genome.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')&(dataset.Ploidy=='Diploidy')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')&(dataset.Ploidy=='Diploidy')]
    x,y=stats.ttest_ind(top.P_cerevisiae, bottom.P_cerevisiae)
    print(env, 't-stat :', x, 'p-value :', y)
    

dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')&(dataset.Ploidy=='Diploidy')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')&(dataset.Ploidy=='Diploidy')]
    x,y=stats.ttest_ind(top.P_cerevisiae, bottom.P_cerevisiae)
    print(env, 't-stat :', x, 'p-value :', y)

dataset=pd.read_csv("../DATA/GLMM_bin_10k_clean.txt",delimiter="\t")

for env in envs:
    top = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Top')&(dataset.Ploidy=='Diploidy')]
    bottom = dataset.loc[(dataset.Environment==env)&(dataset.Top_Bottom=='Bottom')&(dataset.Ploidy=='Diploidy')]
    x,y=stats.ttest_ind(top.S_cerevisiae_S288c, bottom.S_cerevisiae_S288c)
    print(env, 't-stat :', x, 'p-value :', y)
    
#%%
dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")

grps = pd.unique(dataset.Environment.values)

d_data = {grp:dataset['P_cerevisiae'][dataset.Environment == grp] for grp in grps}

F, p = stats.f_oneway(d_data.['caffeine'], d_data['Citric_Acid'], d_data['trt2'])


#%%


cmap = plt.cm.get_cmap('RdBu_r')
dataset=pd.read_csv("../DATA/GLMM_genome.txt",delimiter="\t")
means=dataset.groupby(['Environment'])['P_cerevisiae'].mean().values
sd=dataset.groupby(['Environment'])['P_cerevisiae'].std().values
#%%
colors=[]
for seq in means:
    colors.append(cmap(seq))

plt.figure(figsize=[13,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=16,zorder=0)
real_means=dataset.groupby(['Environment'])['P_cerevisiae'].mean().values
sns.boxplot(x='Environment',y='P_cerevisiae',color='darkgrey',data=dataset,linewidth=2,saturation=0.75)
#plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16'])
#plt.gca().get_legend().remove()
plt.scatter([0,1,2,3,4,5,6,7,8,9],real_means,color='lightgray',marker='D',s=75,edgecolor='black',zorder=2)
plt.xlabel('Environment',weight='bold',size=14)

plt.ylabel('genome hybridity',weight='bold',size=14)
plt.ylim(0.30,0.70)
plt.yticks([0.30,0.35,0.40,0.45,0.50,0.55,0.60,0.65,0.70])

sns.despine(trim=True)
plt.savefig('../Figures/genome_env_hybridity.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%
nothing=[]
means=dataset.mean()
sd=dataset.std()
#%%
for i in range(237):
    nothing.append(0)
plt.figure(figsize=[2,4])
plt.hlines(0.5,linestyles='dashed',xmin=-1,xmax=16,zorder=0)
sns.boxplot(x=nothing,y='P_cerevisiae',color='darkgrey',data=dataset,linewidth=2,saturation=0.75)
plt.xticks([],[])
plt.scatter([0],0.52,color='lightgray',marker='D',s=75,edgecolor='black',zorder=2)
plt.ylabel('genome hybridity',weight='bold',size=14)
plt.ylim(0.30,0.70)
plt.yticks([0.30,0.35,0.40,0.45,0.50,0.55,0.60,0.65,0.70])
sns.despine(trim=True, bottom=True)
plt.savefig('../Figures/genome_allenv_hybridity.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
import random
means=[]
for i in range(0,100000):
    data=[]
    x={}
    for i in range(0,16):
        x[i]=random.uniform(0,1)
    for i in x:
        for j in x:
            if i!=j and i>j:
                data.append(abs(x[i]-x[j]))

    mean=np.mean(data)
    means.append(mean)
#%%
cm = plt.cm.get_cmap('BrBG')

# Get the histogramp
Y,X = np.histogram(means, 40)
x_span = 0.866
C = [cm(((x-0.071)/x_span)) for x in X]
plt.figure(figsize=[6,3])
plt.vlines([0.39,0.37,0.36,0.35,0.34,0.34,0.24,0.22,0.22,0.21],ymin=0,ymax=8000,linestyles='dashed',zorder=0)
plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
plt.xlabel('mean delta chromosome hybridity',weight='bold',fontsize=12)
plt.ylabel('simulations',weight='bold',fontsize=12)
#plt.xticks([0.20,0.25,0.30,0.35,0.40])

sns.despine(trim=True)
plt.savefig('../Figures/RADseq_chr_correlation_sim_mean.png',bbox_inches='tight',dpi=1000)

#%%
cm = plt.cm.get_cmap('BrBG')

# Get the histogramp
Y,X = np.histogram(data, 30)
x_span = 0.866
C = [cm(((x-0.071)/x_span)) for x in X]

plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
plt.xlabel('delta chromosome hybridity',weight='bold',fontsize=12)
plt.ylabel('interactions',weight='bold',fontsize=12)
#plt.yticks([0,5,10])
plt.xticks([0,0.2,0.4,0.6,0.8,1.0])
sns.despine(trim=True,offset=0.5)
#plt.savefig('../Figures/RADseq_chr_correlation_histogram.png',bbox_inches='tight',dpi=1000,transparent=True)



#%%
import random
means=[]
for i in range(0,100000):
    data=[]
    x={}
    for i in range(0,16):
        x[i]=random.uniform(0,1)
    for i in x:
        for j in x:
            if i!=j and i>j:
                data.append(abs(x[i]-x[j]))

    mean=np.mean(data)
    means.append(mean)

cm = plt.cm.get_cmap('BrBG')

# Get the histogramp
Y,X = np.histogram(means, 40)
x_span = 0.866
C = [cm(((x-0.071)/x_span)) for x in X]
plt.figure(figsize=[6,3])
plt.vlines([0.39,0.37,0.36,0.35,0.34,0.34,0.24,0.22,0.22,0.21],ymin=0,ymax=8000,linestyles='dashed',zorder=0)
plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
plt.xlabel('mean delta chromosome hybridity',weight='bold',fontsize=12)
plt.ylabel('simulations',weight='bold',fontsize=12)
#plt.xticks([0.20,0.25,0.30,0.35,0.40])

sns.despine(trim=True)
plt.savefig('../Figures/RADseq_chr_correlation_sim_mean.png',bbox_inches='tight',dpi=1000)