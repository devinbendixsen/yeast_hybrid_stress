#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 15:23:34 2019

@author: devin
"""
# =============================================================================
# Code to plot the delta chromosome hybridity between chromosomes
# =============================================================================
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from scipy import stats
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%% import data supplied by Zebin Zhang from sequencing data
dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")

#%% creates all of the needed dictionaries and lists
corr={}
num=['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16']
for x in num:
    for y in num:
        if float(x)>float(y):
            corr['chr{}_chr{}'.format(x,y)]=[]

samples=[]
for sample in dataset.Sample:
    if sample not in samples:
        samples.append(sample)
        
chronum=[]
for sample in dataset.Chromosome:
    if sample not in chronum:
        chronum.append(sample)

sam_chro={}
for sam in samples:
    for chro in chronum:
        sam_chro['{}_{}'.format(sam,chro)]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']==chro),'P_cerevisiae']

data={}
for seq in sam_chro:
    data[seq]=sam_chro[seq].item()

#%% calculates the mean delta chromosome hybridity (DCH) across ALL ENVIRONMENTS
for seq in data:
    for seq2 in data:
        if seq[:-6]==seq2[:-6] and (float(seq[-2:])>float(seq2[-2:])):
            corr['chr{}_chr{}'.format(seq[-2:],seq2[-2:])].append(abs(data[seq]-data[seq2]))

corrfinal={}
for seq in corr:
    corrfinal[seq]=np.mean(corr[seq])
corr_SE={}
for seq in corr:
    corr_SE[seq]=stats.sem(corr[seq])

#%% plot the distribution of delta chromosome hybridity across ALL ENVIRONMENTS
plt.figure(figsize=[4,2])
data = list(corrfinal.values())
cm = plt.cm.get_cmap('BrBG')
Y,X = np.histogram(data, 30)
x_span = 0.866
C = [cm(((x-0.071)/x_span)) for x in X]

plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
plt.xlabel('delta chromosome hybridity',weight='bold',fontsize=12)
plt.ylabel('interactions',weight='bold',fontsize=12)
#plt.yticks([0,5,10])
plt.xticks([0,0.2,0.4,0.6,0.8,1.0])
sns.despine(trim=True)
plt.savefig('../Figures/RADseq_chr_correlation_histogram.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% plot if there is a correlation between the standard error and mean DCH
y=list(corrfinal.values())
x=list(corr_SE.values())
sns.jointplot(x,y,kind='reg')
plt.xticks()
plt.ylabel('delta chromosome hybridity',weight='bold',fontsize=12)
plt.xlabel('standard error',weight='bold',fontsize=12)
plt.xticks([0.008,0.012,0.016,0.020])
plt.savefig('../Figures/RADseq_chr_correlation_SE_scatter.png',bbox_inches='tight',dpi=1000)#,transparent=True)

#%% plot the heatmap of delta chromosome hybridity (DCH) for ALL ENVIRONMENTS
corr_fig=pd.DataFrame(index=num,columns=num)

for seq in corrfinal:
    x=seq[3:5]
    y=seq[-2:]
    corr_fig.at[x,y]=corrfinal[seq]
print('total DCH : ',sum(corrfinal.values()))
print('DCH mean  : ',np.mean(list(corrfinal.values())))
print('DCH max   : ',max(corrfinal.values()))
print('DCH min   : ',min(corrfinal.values()))

plt.figure(figsize=[12,10])
corr_fig.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(corr_fig,cmap='BrBG',linewidth=1,cbar_kws={"shrink":0.9},square=True,annot=True,vmax=0.937,vmin=0.071)
plt.xlabel('chromosome',weight='bold',fontsize=14)
plt.ylabel('chromosome',weight='bold',fontsize=14)
plt.yticks(rotation=0)
plt.savefig('../Figures/RADseq_chr_correlation_heatmap.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% plot the heatmap of standard error DCH
corr_SE_fig=pd.DataFrame(index=num,columns=num)

for seq in corr_SE:
    x=seq[3:5]
    y=seq[-2:]
    corr_SE_fig.at[x,y]=corr_SE[seq]

plt.figure(figsize=[18,16])
corr_SE_fig.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(corr_SE_fig,cmap='BrBG',linewidth=1,cbar_kws={"shrink":0.9},square=True,annot=True)
plt.xlabel('chromosome',weight='bold',fontsize=14)
plt.ylabel('chromosome',weight='bold',fontsize=14)
plt.yticks(rotation=0)
plt.savefig('../Figures/RADseq_chr_corr_SE_heatmap.png',bbox_inches='tight',dpi=1000)#

#%% create function to plot the heatmap of DCH for each environment
def enviroplot(Environment):
    dataset=pd.read_csv("../DATA/GLMM_Chromosome.txt",delimiter="\t")
    enviroslice=dataset['Environment']==Environment
    dataset=dataset[enviroslice]
    
    corr={}
    num=['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16']
    for x in num:
        for y in num:
            if float(x)>float(y):
                corr['chr{}_chr{}'.format(x,y)]=[]
                
    samples=[]
    for sample in dataset.Sample:
        if sample not in samples:
            samples.append(sample)
    
    chronum=[]
    for sample in dataset.Chromosome:
        if sample not in chronum:
            chronum.append(sample)
    sam_chro={}
    for sam in samples:
        for chro in chronum:
            sam_chro['{}_{}'.format(sam,chro)]=dataset.loc[(dataset['Sample']==sam) & (dataset['Chromosome']==chro),'P_cerevisiae']
    
    data={}
    for seq in sam_chro:
        data[seq]=sam_chro[seq].item()
    
    for seq in data:
        for seq2 in data:
            if seq[:-6]==seq2[:-6] and (float(seq[-2:])>float(seq2[-2:])):
                corr['chr{}_chr{}'.format(seq[-2:],seq2[-2:])].append(abs(data[seq]-data[seq2]))
    
    corrfinal={}
    for seq in corr:
        corrfinal[seq]=np.mean(corr[seq])
    print(Environment)
    print('total DCH : ',sum(corrfinal.values()))
    print('DCH mean  : ',np.mean(list(corrfinal.values())))
    print('DCH max   : ',max(corrfinal.values()))
    print('DCH min   : ',min(corrfinal.values()))
    
    plt.figure(figsize=[4,2])
    data = list(corrfinal.values())
    cm = plt.cm.get_cmap('BrBG')
    Y,X = np.histogram(data, 20)
    x_span = 0.866
    C = [cm(((x-0.071)/x_span)) for x in X]
    
    plt.bar(X[:-1],Y,color=C,width=X[1]-X[0],edgecolor='black',linewidth=1)
    plt.xlabel('delta chromosome hybridity',weight='bold',fontsize=12)
    plt.ylabel('interactions',weight='bold',fontsize=12)
    plt.yticks([0,10,20,30])
    plt.xticks([0,0.2,0.4,0.6,0.8,1.0])
    sns.despine(trim=True)
    plt.savefig('../Figures/RADseq_chr_corr_{}_histogram.png'.format(Environment),bbox_inches='tight',dpi=1000,transparent=True)
    
    corr_fig=pd.DataFrame(index=num,columns=num)
    for seq in corrfinal:
        x=seq[3:5]
        y=seq[-2:]
        corr_fig.at[x,y]=corrfinal[seq]
    
    plt.figure(figsize=[8,8])
    corr_fig.fillna(value=np.nan, inplace=True)
    sns.heatmap(corr_fig,cmap='BrBG',linewidth=1,cbar=False,cbar_kws={"shrink":0.9},square=True,vmax=0.937,vmin=0.071)
    plt.xlabel('chromosome',weight='bold',fontsize=14)
    plt.ylabel('chromosome',weight='bold',fontsize=14)
    plt.yticks(rotation=0)
    #plt.title('{}'.format(Environment),weight='bold',fontsize=15)
    plt.savefig('../Figures/RADseq_{}_heatmap.png'.format(Environment),bbox_inches='tight',dpi=1000,transparent=True)

#%% call enviroplot function
envs=['Caffeine','Citric_Acid','DMSO','EtOH','Hydrogen_Peroxide','Lithium_Acetate','NaCl','Nipagin','Salicylic_Acid','ZnSO4']
for env in envs:
    enviroplot(env)